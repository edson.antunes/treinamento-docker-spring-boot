FROM openjdk:12
ADD target/treinamento-docker-spring-boot.jar treinamento-docker-spring-boot.jar
EXPOSE 8088
ENTRYPOINT ["java", "-jar", "treinamento-docker-spring-boot.jar"]
