package treinamentodockerspringboot.treinamentodockerspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreinamentoDockerSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreinamentoDockerSpringBootApplication.class, args);
	}

}
